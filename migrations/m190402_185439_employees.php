<?php

use yii\db\Migration;

/**
 * Class m190402_185439_employees
 */
class m190402_185439_employees extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('employees', [
            'id' => $this->primaryKey(),
            'email' => $this->string(),
            'surname' => $this->string(),
            'name' => $this->string(),
            'patronymic' => $this->string(),
            'photo' => $this->string(),
            'department_id' => $this->integer(),
        ]);

        $this->createIndex(
            'idx-employees-department_id',
            'employees',
            'department_id'
        );

        $this->addForeignKey(
            'fk-employees-department_id',
            'employees',
            'department_id',
            'departments',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('employees');
    }

}
