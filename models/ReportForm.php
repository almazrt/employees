<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ReportForm extends Model
{
    public $departmentId;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['departmentId'], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'departmentId' => 'Department',
        ];
    }

}
